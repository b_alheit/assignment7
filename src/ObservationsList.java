import java.lang.Iterable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Collections;

public class ObservationsList implements Iterable<Observation>{

    private List<Observation> observations;



    /**
     * Create a new Observations object.
     */
    public ObservationsList()  {
        observations = new ArrayList<Observation>();
    }



    /**
     * Record a new observation, appending the given registration to the existing sequence.
     * @param observation the registration of the vehicle observed.
     */
    public void record(Observation observation) {
        observations.add(observation);
    }

    public void record(Registration reg, Time time) {observations.add(new Observation(reg, time));}

    /**
     * Obtain the total number of observations.
     */
    public int getTotal() { return observations.size(); }

    /**
     * Obtain a list of the vehicles which have been observed.
     */
    public List<Registration> getVehicles() {
        List<Registration> results = new ArrayList<Registration>();
        for(Observation observed : observations) {
            if (! results.contains(observed.getIdentifier())) {
                results.add(observed.getIdentifier());
            }
        }
        return results;
    }

    public ObservationsList getObservations(final Registration identifier) {
        ObservationsList newObservations = new ObservationsList();
        for(Observation observed : observations) {
            if (identifier.equals(observed.getIdentifier())) {
                newObservations.record(observed);
            }
        }
        return newObservations;
    }

    public ObservationsList getObservations(final Time s, final Time e) {
        ObservationsList newObservations = new ObservationsList();
        for(Observation observed : observations) {
            if (observed.inPeriod(s, e)) {
                newObservations.record(observed);
            }
        }
        return newObservations;
    }


    /**
     * Obtain an iterator that can be used to view the observations one by one.
     */
    public Iterator<Observation> iterator() {
        ArrayList<Observation> out = new ArrayList<Observation>(observations);
        Collections.sort(out);
        return out.iterator();
    }

}
