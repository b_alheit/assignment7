import java.lang.Comparable;

public class Observation implements Comparable<Observation>{
//public class Observation{
    private final Registration reg;
    private final Time time;

    public Observation(final Registration registration, final Time time){
        this.reg = registration;
        this.time = time;
    }

    public Time getTime(){return time;}

    public Registration getIdentifier(){return reg;}

    public boolean isFor(final Registration identifier){return reg.equals(identifier);}

    public boolean inPeriod(final Time s, final Time e){return s.compareTo(time) <= 0 && e.compareTo(time) >= 0;}

    public boolean equals(Object o){
        if(o.getClass() != this.getClass()) return false;
        else{
            Observation other = (Observation) o;
            return this.reg.equals(other.reg) && this.time.equals(other.time);
        }
    }

    public int compareTo(Observation other){return time.compareTo(other.getTime());}

    public String toString(){return "["+time.toString()+", "+reg.toString()+"]";}
}
